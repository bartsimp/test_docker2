FROM centos:7

#RUN yum -y update

COPY entrypoint.sh /entrypoint.sh

CMD echo "Hello from CMD"
#CMD [ "/entrypoint.sh" ]

ENTRYPOINT echo "Hello from ENTRYPOINT"
